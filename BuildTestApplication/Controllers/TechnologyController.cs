﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BuildTestApplication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BuildTestApplication.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TechnologyController : ControllerBase
    {
        public IEnumerable<Technology> GetTechnologies()
        {
            List<Technology> technos = new List<Technology>();
            Technology technology1 = new Technology
            {
                Level = (int)Enums.Level.CONIRMED,
                TechnoName = "Angular",
                NumberYearOfExperience = 2
            };

            Technology technology2 = new Technology
            {
                Level = (int)Enums.Level.CONIRMED,
                TechnoName = ".Net",
                NumberYearOfExperience = 6
            };

            Technology technology3 = new Technology
            {
                Level = (int)Enums.Level.CONIRMED,
                TechnoName = "SQL",
                NumberYearOfExperience = 6
            };
            technos.Add(technology1);
            technos.Add(technology2);
            technos.Add(technology3);

            return technos;
        }

    }
}