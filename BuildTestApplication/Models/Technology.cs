﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuildTestApplication.Models
{
    public class Technology
    {
        public string TechnoName { get; set; }
        public int Level { get; set; }
        public int NumberYearOfExperience { get; set; }
    }
}
