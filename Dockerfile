FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["BuildTestApplication/BuildTestApplication.csproj", "BuildTestApplication/"]
RUN dotnet restore "BuildTestApplication/BuildTestApplication.csproj"
COPY . .
WORKDIR "/src/BuildTestApplication"
RUN dotnet build "BuildTestApplication.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "BuildTestApplication.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "BuildTestApplication.dll"]
